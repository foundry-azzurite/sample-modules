Contains my modules by default. You should just remove them from the `modules.json` if you don't want them.

An empty `modules.json` contains exactly this: `[]`